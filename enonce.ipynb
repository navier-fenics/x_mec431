{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "f69589d6-78ed-431f-bea0-a23f9891f8bd",
   "metadata": {},
   "source": [
    "<h1 align=\"center\"> PC9 : mise en oeuvre numérique de la méthode des éléments finis </h1>\n",
    "<h2 align=\"center\"> Mercredi 15 mai 2024 </h2>\n",
    "\n",
    "## Introduction et instructions\n",
    "\n",
    "L'objet de cette mise en oeuvre est de pratiquer l'utilisation de calculs éléments finis en *élasticité linéaire HPP* à l'aide du logiciel [FEniCSx](https://fenicsproject.org/). De nombreuses fonctions sont déjà préimplémentées, un descriptif est fourni ci-dessous.\n",
    "\n",
    "### Instructions préliminaires\n",
    "\n",
    "**IMPORTANT**: Dans l'arborescence JupyterHub, sélectionner le dossier `MEC431_PC9`, cliquer sur `\"Move\"` et entrer dans la boîte de dialogue `\"/persistent/\"` afin de déplacer le dossier `MEC431_PC9` dans votre dossier de sauvegarde permanente afin de conserver la trace de votre travail après votre déconnexion.\n",
    "\n",
    "### Liens vers:\n",
    "\n",
    "* [**Notebook de travail**](barrage_Ternay.ipynb)\n",
    "* [Descriptif des fonctions disponibles](documentation.ipynb) \n",
    "\n",
    "### Rendu\n",
    "\n",
    "Le travail peut être rendu, au choix, sous forme de **rapport Word/PDF** contenant des images de votre implémentation et vos résultats ou bien sous la forme d'un **notebook** (commentaires pour les réponses aux questions, scripts et tracés des solutions).\n",
    "\n",
    "## Le barrage du Ternay\n",
    "\n",
    "On revisite la problématique du calcul des déplacements et des contraintes au sein d'un barrage poids, sur une géométrie plus réaliste correspondant à celle du *barrage du Ternay* situé à Saint-Marcel-lès-Annonay en Ardèche (Fig. 1).\n",
    "\n",
    "<img src=\"pic/barrage_Ternay.jpg\" alt=\"Le barrage du Ternay avant l'ajout d'enrochements à l'aval\" width=\"600\" align=\"middle\">\n",
    "\n",
    "**Fig.1** : Carte postale du barrage du Ternay avant l'ajout d'enrochements à l'aval\n",
    "                                                                                  \n",
    "Le barrage a été construit en 1858 pour alimenter Annonay en eau potable. Il s'agit d'un barrage poids en maçonnerie d'une hauteur d'environ $40$ m pour une base $W=25$ m. Le barrage est légèrement courbe et long d'environ 150 m dans la direction $Z$. Le profil plan de sa section courante est représenté sur la Figure 2.\n",
    "\n",
    "<img src=\"pic/Ternay_geometry_2d.svg\" alt=\"Section du barrage\" width=\"400\" align=\"middle\">\n",
    "\n",
    "**Fig.2** : Section courante 2d du barrage dans le plan (OXY).\n",
    "\n",
    "On se propose de réaliser différents calculs élastiques sur cette géométrie. On comparera notamment différents cas de charges, modélisation géométriques et propose une démarche (simplifiée) de vérification du dimensionnement du barrage.\n",
    "\n",
    "## Hypothèses générales de modélisation\n",
    "\n",
    "On considère que le matériau constitutif du barrage est homogène isotrope, de module d'Young $E=20$ GPa, de coefficient de Poisson $\\nu=0.2$ et de densité volumique $\\rho_b g = 23.5 \\text{ kN/m}^3$. Pour l'eau, $\\rho g = 9.81 \\text{ kN/m}^3$.\n",
    "\n",
    "En conditions normales, la hauteur d'eau est de $H=33$ m. En condition rare, la plus haute hauteur d'eau est $H=37$m.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "73f22ac7-7d67-40c2-be8f-053530dfef1f",
   "metadata": {},
   "source": [
    "## Questions\n",
    "\n",
    "### 1. Cas de charge \"lac plein\"\n",
    "\n",
    "On réalise tout d'abord un calcul 2D, la géométrie étant donnée en mètres par la fonction `generate_2D_mesh`. La numérotation des différentes régions du bord est représentée sur la Figure 3. \n",
    "\n",
    "\n",
    "<img src=\"pic/Ternay_regions_2d.svg\" alt=\"Numérotation du bord\" width=\"200\" align=\"middle\">\n",
    "\n",
    "**Fig.3.**: Numérotation des régions du bord du domaine\n",
    "\n",
    "Le cas de charge du lac plein, tel qu'illustré sur la Figure 2 consiste en le poids du barrage et la pression de l'eau sur le parement amont ($H=33$ m).\n",
    "\n",
    "**1.1** Mettre en donnée le problème pour le cas de charge considéré. Vous devez pour cela compléter les valeurs des paramètres matériau dans le dictionnaire `material_data` (on choisira des unités cohérentes et appropriées). Vous devez ensuite compléter la loi de comportement du matériau exprimée par la fonction `sigma(eps)` en fonction des coefficients de Lamé `lmbda` et `mu` déjà définis et des opérateurs symboliques fournis par `ufl` ([cf. ici](documentation.ipynb#Opérateurs-symboliques-ufl)). Enfin, vous devez également compléter l'expression du potentiel des efforts extérieurs par la fonction `Phi(v)`, `v` étant un champ de déplacement test quelconque, cinématiquement admissible. On utilisera pour cela les objets `f` et `p` représentant la force volumique et la pression du fluide, le vecteur normal extérieur unitaire `n` ainsi que les mesures d'intégrations de volume `dx` et de surface `ds(i)`.\n",
    "\n",
    "\n",
    "**1.2** Effectuer le calcul et commenter l'allure de la déformée. Commenter également l'allure des différentes composantes de la contrainte, sont-elles conformes à votre intuition ?\n",
    "\n",
    "**1.3** Reporter l'évolution de l'énergie potentielle en fonction de la finesse de maillage (par ex. entre lcar=4 m et lcar=0.2 m) pour une interpolation éléments finis linéaire (`order=1`) et quadratique (`order=2`). Commenter les résultats. Qu'en est-il de l'évolution des valeurs maximales des déplacements et des contraintes, par ex: $\\sigma_{yy}$ ?\n",
    "\n",
    "Dans la suite on fixera une taille de maille `lcar` appropriée."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9c12e49d-892c-43f6-ab35-b54afa00e819",
   "metadata": {},
   "source": [
    "### 2. Cas de charge \"lac vide\"\n",
    "\n",
    "On suppose que le lac est vide. Sous l'action du soleil, on considère que le barrage est soumis à un échauffement homogène de $\\Delta = +10°C$. On prendra un coefficient de dilatation thermique $\\alpha=10^{-5} /\\:^\\circ\\text{C}$.\n",
    "\n",
    "**2.1** A partir de la loi de comportement thermoélastique isotrope $\\underline{\\underline{\\sigma}}(\\underline{\\epsilon},\\Delta T)$, montrer que la condition d'optimalité du principe de l'énergie potentielle:\n",
    "\n",
    "$$\\dfrac{\\partial (W_\\text{el}-\\Phi)}{\\partial \\underline{\\xi}}\\cdot\\underline{\\widehat{\\xi}} = \\int_{\\Omega} \\underline{\\underline{\\sigma}}(\\underline{\\underline{\\varepsilon}},\\Delta T):\\underline{\\underline{\\widehat{\\varepsilon}}}\\text{d}\\Omega - \\Phi(\\underline{\\widehat{\\xi}}) = 0 \\quad \\forall \\underline{\\widehat{\\xi}}\\in \\mathcal{C}_\\text{ad}$$\n",
    "peuvent également se réécrire:\n",
    "$$\\int_{\\Omega} \\underline{\\underline{\\sigma}}(\\underline{\\underline{\\varepsilon}}):\\underline{\\underline{\\widehat{\\varepsilon}}}\\text{d}\\Omega - \\Phi_\\text{th}(\\underline{\\widehat{\\xi}}) = 0 \\quad \\forall \\underline{\\widehat{\\xi}}\\in \\mathcal{C}_\\text{ad}$$\n",
    "\n",
    "où $\\underline{\\underline{\\sigma}}(\\underline{\\underline{\\varepsilon}})$ est la loi de comportement élastique isotherme et où $\\Phi_\\text{th}$ est un potentiel des efforts extérieurs tenant également compte de l'effet des déformations thermiques engendrées par l'échauffement $\\Delta T$.\n",
    "\n",
    "**2.2** Modifier alors le potentiel `Phi(v)` avec l'expression trouvée pour $\\Phi_\\text{th}$ et calculer la déformée tenant compte de cet échauffement (sans la pression de l'eau). Discuter de l'allure de la déformée obtenue et de l'amplitude des déplacements trouvées par rapport au cas de charge précédent."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "078b954f-e66b-4f99-81a3-567753581f4a",
   "metadata": {},
   "source": [
    "### 3. Modélisation 3D\n",
    "\n",
    "On modélise à présent le barrage en 3D qui présente une légère courbure dans sa direction longitudinale. On considère que les deux extrémités sont encastrées dans une roche très raide de sorte que le déplacement y est bloqué.\n",
    "\n",
    "**3.1** Effectuer le calcul 3D des deux cas de charge précédents et commenter les valeurs trouvées pour les déplacements par rapport à la modélisation 2D. \n",
    "\n",
    "\n",
    "**3.2** Que se passe-t-il si on enlève les conditions aux limites aux deux extrémités (`fix_extremities=False` comme argument de `generate_3D_mesh`)?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "174d58d7-4a76-4875-9d1c-630a4ec7570b",
   "metadata": {},
   "source": [
    "### 4.Vérification au glissement en conditions rares\n",
    "\n",
    "On repasse à présent à la modélisation 2D pour le cas de charge \"lac plein\" pour une hauteur d'eau défavorable $H=37$ m. On cherche ici à vérifier la stabilité du barrage vis-à-vis du glissement de sa base sous l'action des charges associées.\n",
    "\n",
    "**4.1** Compte-tenu des différents efforts extérieurs appliqués et de l'équilibre global du barrage, calculer les composantes horizontales et verticale de la résultante des efforts à la base du barrage. On se placera dans le cas général où les forces volumiques admettent également une composante horizontale (cf. Section 5.).\n",
    "\n",
    "> *Note*: On pourra se servir de la fonction `integrate(a*dx + b*ds(i))` avec `dx` représentant l'intégration sur l'ensemble du volume et `ds(i)` l'intégration sur le bord numéroté i du domaine.\n",
    "\n",
    "**4.2** Une première vérification de la condition de non-glissement du barrage repose sur l'écriture d'un critère de frottement formulé sur la résultante globale des efforts à la base du barrage. Exprimer le critère portant sur le coefficient de frottement $\\tan\\phi$ et les composantes de la résultante et vérifier que ce dernier est bien satisfait pour une valeur typique de $\\phi=40°$.\n",
    "\n",
    "**4.3** Une deuxième vérification du non-glissement consiste à étudier, le long de la base du barrage, l'évolution du critère de Coulomb:\n",
    "$f(\\sigma_n,\\tau) = \\sigma_n \\tan\\phi + |\\tau|$\n",
    "où $\\sigma_n$ est la contrainte normale agissant sur la facette et $|\\tau|$ la contrainte de cisaillement. Compte-tenu des singularités de contrainte à la pointe de la base, on considérera que la condition de non-glissement est satisfaite tant que la distance sur laquelle le critère n'est pas vérifié $f(\\sigma_n,\\tau)>0$  (interface fissurée)  est inférieure à 25% de la largeur totale de la base."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aa5d397f-a2a0-4763-beb5-dd28c7bef3af",
   "metadata": {},
   "source": [
    "### 5. Vérification au glissement en conditions sismiques acidentelles\n",
    "\n",
    "On considère à présent une situation accidentelle pour le cas de charge du \"lac plein\", comprenant:\n",
    "* une hauteur d'eau normale de $H=33m$\n",
    "* l'action supplémentaire d'un séisme dans la direction $\\underline{e}_x$. \n",
    "\n",
    "Concernant le séisme, une première approche simplifiée d'analyse, dite *pseudo-statique*, consiste à ajouter une composante horizontale d'intensité $ag\\underline{e}_x$ au vecteur gravité, $ag$ étant l'accélération considérée exprimée en g. De plus, la mise en mouvement du lac génère également une surpression hydrosismique supplémentaire donnée par la règle empirique de Westergaard:\n",
    "\\begin{equation}\n",
    "\\Delta p(Y) = \\frac{7}{8}\\rho a g \\sqrt{H(Y-H)}\n",
    "\\end{equation}\n",
    "\n",
    "5.1 L'Ardèche se situant dans une zone d'aléa sismique faible à modéré, l'accélération de pointe maximale dans cette région varie de $0.1g$ à $0.17g$. Que concluez-vous ?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "84a98489-fb3a-496d-ac20-d5755ccc1759",
   "metadata": {},
   "source": [
    "### 6. Analyse de sensibilité à la présence d'une fissure\n",
    "\n",
    "On étudie à présent le champ de contrainte pour le cas de charge \"lac plein\", en situation accidentelle ($H=37$ m), sans séisme en présence d'une fissure horizontale débouchant sur le parement amont à une hauteur $y_\\text{crack}=25$ m. La fissure fait une longueur $a_\\text{crack}=1$ m et est modélisée comme une demi-ellipse de très faible ouverture $e_\\text{crack}=2$ cm. La nouvelle numérotation du bord en présence de la fissure est représentée Fig. 4.\n",
    "\n",
    "<img src=\"pic/Ternay_crack_2d.svg\" alt=\"Numérotation du bord\" width=\"600\" align=\"middle\">\n",
    "\n",
    "**6.1** Comparer le comportement global du barrage (déplacement maximal, contrainte à la base) avec et sans la présence de la fissure. \n",
    "\n",
    "**6.2** Reporter l'évolution de la valeur maximale de la contrainte $\\sigma_{yy}$ en fonction de la finesse du maillage utilisé. Que concluez-vous ?\n",
    "\n",
    "**6.3** On cherche à évaluer le risque de propagation de cette fissure. Un critère très sommaire consiste à étudier le signe de la contrainte $\\sigma_{yy}$ en avant de la pointe de fissure. On supposera qu'une contrainte de compression empêche la fissure de ce propager. Qu'en est-il ici ?\n",
    "\n",
    "**6.4** La fissure étant légèrement ouverte, elle est également soumise à la pression de l'eau. Modifier les surfaces sur lesquelles s'appliquent les efforts de pression et analyser à nouveau la possibilité ou non d'une propagation."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
